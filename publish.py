import paho.mqtt.client as mqtt
import time
import argparse
import os
from Count_module import config
#========================================================================= 


def on_connect(client, userdata, flags, rc) :
    print ("on_connect()")

#=========================================================================
def on_publish(client, userdata, mid) :
    print ("on_publish()")

#=========================================================================
user = "ienergy"                    #Connection username
password = "ienergy123"             #Connection password

def send() :
    mqttc = mqtt.Client()
    mqttc.username_pw_set(user, password=password)
    mqttc.on_connect = on_connect
    mqttc.on_publish = on_publish

    #host      = "AWS Host"
    host      = "52.207.136.13"
    port      = 1883
    keepalive = 60

    print ("\nConnect to '%s', port '%s', keepalive '%s'" % (host, port, keepalive))
    mqttc.connect(host=host, port=port, keepalive=keepalive)

    time.sleep(3)        
    mqttc.loop_start()    
    time.sleep(3)

    topic = "anganwadi"
    f= open("Count_module/config.py","r")
    msg = "People "+f.read()
    print ("Publish to '%s' msg '%s'" % (topic, msg))
    mqttc.publish(topic, msg, qos=2)
    time.sleep(3)   
    mqttc.loop_stop()
# end send()

#=========================================================================
if __name__ == "__main__":
    send()
# end if