import paho.mqtt.client as mqttClient
import time
from datetime import datetime
from yolo_wifi import config
def on_connect(client, userdata, flags, rc):

    if rc == 0:

        print("Connected to broker")

        global Connected                #Use global variable
        Connected = True                #Signal connection

    else:

        print("Connection failed")

def on_message(client, userdata, message):
    print ("Message received: from Anganwadi "+config.anganwadi + str(message.payload))
    with open('file.txt','a+') as f:
         f.write("Message received:  from Anganwadi "+config.anganwadi+" at "+datetime.now().strftime('%Y-%m-%d %H:%M:%S')+" "+
                  str(message.payload) + "\n")

Connected = False   #global variable for the state of the connection

broker_address= "52.207.136.13"  #Broker address
port = 1883                         #Broker port
user = "ienergy"                    #Connection username
password = "ienergy123"            #Connection password

client = mqttClient.Client("Python")               #create new instance
client.username_pw_set(user, password=password)    #set username and password
client.on_connect= on_connect                      #attach function to callback
client.on_message= on_message                      #attach function to callback
client.connect(broker_address,port,60) #connect
client.subscribe("anganwadi") #subscribe
client.loop_forever() 