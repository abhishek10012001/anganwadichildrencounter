import paho.mqtt.client as mqtt
import sys
import time
from zipfile import ZipFile
import io
import os

print("Code Started")
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("ienergyiot")

# The callback for when a PUBLISH message is received from the server.
dir_raw = 'raw_data'
dir_unzip = 'unzipped_data' 
def on_message(client, userdata, msg):
    global start
    #print(msg.topic+" "+str(msg.payload))
    N = len(msg.payload)
    i = 0
    while i < N:
        if chr(msg.payload[i]) != '\n':
            pass
        else:
            break
        i = i + 1
    file_name = msg.payload[0:i]
    start = time.perf_counter()
    print("\nFile Name : " + str(file_name))
    data = msg.payload[i+1:]
    f = open(dir_raw+'\\'+file_name.decode(), 'wb')
    f.write(data)
    f.close()
    #print("Data : " + str(data))
    binary_stream = io.BytesIO()
    binary_stream.write(data)
    zipObj = ZipFile(binary_stream, 'r')
# Extract all the contents of zip file in current directory
    zipObj.extractall(dir_unzip)   
    


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

if os.path.exists(dir_raw):
    pass
else:
    os.makedirs(dir_raw) 
if os.path.exists(dir_unzip):
    pass
else:
    os.makedirs(dir_unzip) 
    
client.username_pw_set(username="ienergy",password="ienergy123") # password
client.connect("52.207.136.13")

client.loop_start() 

print("Code running")
i = 0
start = time.perf_counter()
while 2 > 1:
    time.sleep(1)
    i = time.perf_counter() - start
    # if i > 60:
    #  start = time.perf_counter()
    sys.stdout.write('\r'+str(i))
    pass